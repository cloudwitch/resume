# Fiona Rose Buckner

[Gitlab](https://gitlab.com/cloudwitch) | [Personal Website](https://cloudwitch.dev/)

## Who am I

Fun loving individual who just wants to do something cool with her life.

## Particular set of skills

- Cloud-native development
- Containers
- Python
- Unix
- Linux
- Highly available clustering
- Networking
- Electronics assembly and repair

## Where I've been

**Sr. AWS Cloud Engineer Consultant | Onica, a Rackspace Company | Santa Monica, CA | September 2018 - Current**

Engineer and Architect customer projects within the AWS Cloud. Develop Infrastructure as Code (IaC) through Cloudformation and Terraform. Project highlights include but are not limited to:

- Automating critical components of Disaster Recovery using CloudEndure, Lambda, Python, Step Functions, and Systems Manager for a major clothing company to facilitate cost savings through reducing colocation costs.
- Developed near-zero downtime Autoscale Group migration for Southwest Airlines’s EKS clusters through Python and Groovy Pipelines in Jenkins. This cut costs while improving availability by leveraging multiple EC2 worker instance types through a mix of spot and reserved instances.
- Assisted in build out of ELK stack cluster designed to ingest mission critical application logs from all Southwest Airlines AWS accounts.
- Solo engineer on a project for a major newspaper company to design and deploy landing zones as part of the first phase of on-premise data center exit. Duties included multi-account networking leveraging Transit Gateway for communication with legacy applications over VPN. Automation of AMIs through Hashicorp Packer for Windows and Linux operating systems. Creation of Cloudformation Templates for the brownfield migration of existing workloads.
- Designed and deployed a landing zone, highly available Graylog monitoring cluster, and AMI automation through Packer to facilitate data center exit operations during a company acquisition for a major real estate company.

**Sr. Cloud Analyst | Tyson Foods Inc. | Springdale, AR| January 2018 - August 2018**

Architected and developed application migration methods to the AWS and GKE public clouds.

- Assisted customers with designing the most efficient use of cloud services in efforts to modernize traditional applications while adopting Agile workflow.
- Spearheaded Terraform Infrastructure as Code (IaC) standardization in preparation for a multi-cloud landscape.
- Utilized Cloud Formation, Terraform, and Packer to deploy customized IaC application stacks.
- Conducted classes on Terraform, Docker, and cloud-native development.

**Sr. Linux Technical Analyst | Tyson Foods Inc. | Springdale, AR| June 2016 - January 2018**

Administered and built Suse Linux Enterprise (SLES) and Red Hat Enterprise Linux (RHEL) servers for Tyson Foods.

- Utilized Saltstack and Gitlab CI to develop automation for day to day tasks.
- Developed, documented, and standardized procedures to maintain quality of service for the business.
- Designed, built, and managed Hadoop clusters based on Hortonworks HDP and HDF.
- Architected High Availability Linux systems utilizing Corosync Linux Clustering Engine while also leveraging VMware for increased uptime.
- Troubleshot hardware and software issues through use of Splunk monitoring.
- Assisted in roll out and upgrade tasks for Oracle, SAP Hana and other business critical applications.

**Level 2 Linux/Unix Systems Administrator | CompuCom Systems Inc. | Bentonville, AR | June 2014 - June 2016**

Provided Linux and Unix administration for 50,000+ RHEL, SLES, HP-UX, and AIX based systems for the Walmart Home Offices, Distribution Centers, Stores

- Administered cluster services such as DRBD, Pacemaker, Corosync, and Serviceguard.
- Troubleshot and engineered solutions for software, virtualization, and hardware issues by analyzing log files and symptoms described by the end users.
- Assisted in troubleshooting Windows server guest virtual machines in Xen and VMWare virtualization clusters.
- Carried out projects including software/operating system installation, upgrades and modification.
- Common daily tasks include performance tuning, resource optimization, monitoring, and assisting application teams troubleshoot their own applications.
- Minor troubleshooting roles include but are not limited to; DHCP/DNS servers, IP address management, wired and wireless network troubleshooting, wireless handheld device troubleshooting, workstations, thin clients, printers, print servers, and point of sale troubleshooting.

**Staff Sergeant Select Electro-Mechanical Maintenance Team Chief, Minuteman III Weapon System | United States Air Force | Malmstrom Air Force Base, MT | September 2008 - September 2012**

Led a team of three technicians to perform maintenance and troubleshooting on power systems, guidance systems, data acquisition systems, wiring systems, security systems on ground, aerial and support equipment of remote Minuteman III missile sites. Troubleshot, repaired or replaced, circuit boards, and equipment drawers as needed to keep the entire missile system operational. Loaded launch programming software onto missile guidance systems. Navigated Montana countryside in order to arrive as safely and quickly as possible to remote missile sites.

Notable achievements and accomplishments:

- Minuteman III missile test launch July 27, 2011 Vandenberg AFB, California. Sent on temporary duty assignment June 28 to Aug. 1, 2011 to participate in the set-up, test, and launch.
- Maintained 100% “Satisfactory” rating throughout all QA evaluations.
- Awarded Staff Sergeant eligibility after passing promotion promotion test on the first attempt.

## Education

- DevOps Implementation Boot Camp (ICP-FD0) February 2018
- AWS Certified Solutions Architect - Associate December 21, 2017 to December 21, 2019
- University of Arkansas, Mechanical Engineering, September 2012 to May 2014
- Missile Maintenance Course, Vandenberg AFB, California November 2008 to March 2009
- Electronics Familiarization Course, Keesler AFB, Mississippi, October 2008 to November 2008
- Air Force Basic Training, Lackland AFB, Texas, September 2008 to October 2008
- Etiwanda High School Fontana, California graduated June 2008
